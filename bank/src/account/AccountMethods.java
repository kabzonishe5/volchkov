package account;

public interface AccountMethods {
    boolean getAccountLock();

    void setAccountLock();

    void setAccountUnlock();

    void setUserName(String userName);

    void setPassword(String password);

    Tariff getTariff();

    void setTariff(Tariff tariff);

    String getNumber();

    PeopleD getPeopleD();

    int getBalance();

    void setBalance(int rub);


    String printInfo();

    void printInfoLock();


}
