package account;

public class HistoryPayment {
    private Payment[] historyPayments;

    HistoryPayment() {
        this.historyPayments = new Payment[1];
    }

    public void addPayment(Payment payment) {
        if (historyPayments[0] == null) {
            historyPayments[0] = payment;
        } else {
            Payment[] history = new Payment[historyPayments.length + 1];
            for (int i = 0; i < historyPayments.length; i++) {
                history[i] = historyPayments[i];
            }
            history[historyPayments.length] = payment;
            this.historyPayments = new Payment[history.length];
            for (int i = 0; i < history.length; i++) {
                historyPayments[i] = history[i];
            }
        }
    }

    public void printHistory() {
        for (int i = 0; i < historyPayments.length; i++) {
            System.out.println(historyPayments[i].toString());
        }
    }
}
