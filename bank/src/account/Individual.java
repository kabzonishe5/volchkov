package account;

public class Individual implements AccountMethods {
    private String userName;
    private String password;
    private int balance;
    private String number;
    private boolean numberLock;
    private boolean accountLock;
    private PeopleD peopleD;
    private Tariff tariff;

    Individual(String userName, String password, int balance, String number, boolean numberLock, boolean accountLock,
               PeopleD peopleD, Tariff tariff) {
        this.accountLock = accountLock;
        this.balance = balance;
        this.peopleD = peopleD;
        this.number = number;
        this.numberLock = numberLock;
        this.password = password;
        this.tariff = tariff;
        this.userName = userName;
    }

    @Override
    public void setBalance(int rub) {
        this.balance =this.balance+rub;
    }

    @Override
    public int getBalance() {
        return balance;
    }

    @Override
    public PeopleD getPeopleD() {
        return peopleD;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public void setTariff(Tariff tariff) {
        this.tariff = tariff;
    }

    @Override
    public Tariff getTariff() {
        return tariff;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public void setAccountLock() {
        accountLock = true;
    }

    @Override
    public void setAccountUnlock() {
        accountLock = false;
    }

    @Override
    public boolean getAccountLock() {
        return accountLock;
    }

    @Override
    public void printInfoLock() {

    }

    @Override
    public String printInfo() {
        return "Информация о клиенте: " + number + " username:" + userName + " balance: " + balance + " тариф: " + tariff;
    }
}
