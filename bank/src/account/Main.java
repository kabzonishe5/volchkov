package account;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws ParseException {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Добро пожаловать в салон связи, сначала Вам нужно создать договор об оказании услуг.");
        System.out.println("Введите ФИО:");
        String name = scanner.nextLine();
        System.out.println("Дату рождения(день/месяц/год):");
        String date = scanner.nextLine();
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(date);
        System.out.println("Адрес проживания: ");
        String address = scanner.nextLine();
        System.out.println("Вы физическое(1) или юридическое(2) лицо?(ответ 1 или 2)");
        int people = Integer.parseInt(scanner.nextLine());
        PeopleD user = null;
        if (people == 1) {
            user = new PeopleD(name, 1, address, date1, People.INDIVIDUAL);
        } else if (people == 2) {
            user = new PeopleD(name, 1, address, date1, People.IP);
        } else {
            System.out.println("Ошибка ввода");
        }
        System.out.println(user.toString() + " спасибо за заключение договора.");
        System.out.println("Теперь нужно пройти регистрацию клиента.");
        System.out.println("Введите имя пользователя:");
        String username = scanner.nextLine();
        System.out.println("Введите пароль:");
        String password = scanner.nextLine();
        System.out.println("Введите желаемый номер телефона(+7-xxx-xxx-xx-xx):");
        String number = scanner.nextLine();
        while (number.length() != 16 && number.length() != 12) {
            System.out.println("Неправильный формат телефона.");
            System.out.println("Введите еще раз: ");
            number = scanner.nextLine();
        }
        System.out.println("Выберите из продложенных, тариф, который хотите установить: ");
        TariffList tariffList = new TariffList();
        tariffList.printTariffs();
        System.out.println("Введите номер тарифа: ");
        int tariff = Integer.parseInt(scanner.nextLine());
        AccountMethods individual = null;
        if (tariff < 4 && tariff > 0) {
            switch (tariff) {
                case 1:
                    if (people == 1) {
                        individual = new Individual(username, password, 0, number, false,
                                false, user, tariffList.getTariff(0));
                    } else {
                        individual = new Ip(username, password, 0, number, false, false,
                                user, tariffList.getTariff(0));
                    }
                    break;
                case 2:
                    if (people == 1) {
                        individual = new Individual(username, password, 0, number, false,
                                false, user, tariffList.getTariff(1));
                    } else {
                        individual = new Ip(username, password, 0, number, false, false,
                                user, tariffList.getTariff(1));
                    }
                    break;
                case 3:
                    if (people == 1) {
                        individual = new Individual(username, password, 0, number, false,
                                false, user, tariffList.getTariff(2));
                    } else {
                        individual = new Ip(username, password, 0, number, false, false,
                                user, tariffList.getTariff(2));
                    }
                    break;

            }
        } else {
            System.out.println("Неверный номер тарифа.");
        }
        System.out.println("Поздравляем, теперь вы зарегестрированы как клиент.");
        System.out.println(individual.printInfo());
        System.out.println("Добро пожаловать в главное меню своего аккаунта.");
        System.out.println("Вот ряд услуг и возможностей которые вы можете совершить:");
        HistoryPayment historyPayment=new HistoryPayment();
        while (true) {
            System.out.println("1. Заблокировать или разблокировать счет . ");
            System.out.println("2. Редактировать учетную запись абонента. ");
            System.out.println("3. Получить актуальную информацию по номеру абонента. ");
            System.out.println("4. Проверить состояние баланса и остаток трафика. ");
            System.out.println("5. Пополнить баланс. ");
            System.out.println("6. Посмотреть историю платежей. ");
            System.out.println(" Введите номер операции:");

            int change = Integer.parseInt(scanner.nextLine());
            if (change == 1) {
                if (!individual.getAccountLock()) {
                    System.out.println("Ваш аккаунт открыт,желаете заблокировать его?(да/нет)");
                    String value = scanner.nextLine();
                    if (value.equals("да")) {
                        individual.setAccountLock();
                        System.out.println("Вы заблокировали свой аккаунт.");
                    } else if (value.equals("нет")) {
                        System.out.println("Спасибо что остаетесь с нами,хорошего дня.");
                    } else {
                        System.out.println("Ответ неверный.");
                    }
                } else {
                    System.out.println("Ваш аккаунт заблокирован, желаете снова открыть его?");
                    String value = scanner.nextLine();
                    if (value.equals("да")) {
                        individual.setAccountUnlock();
                        System.out.println("С возвращением, вы можете снова пользоваться нашими услугами.");
                    } else if (value.equals("нет")) {
                        System.out.println("Ваш аккаунт остается заблокированным, ждет вас снова.");
                    } else {
                        System.out.println("Ответ неверный.");
                    }
                }
            }
            if (change == 2 && !individual.getAccountLock()) {
                System.out.println("Редактирование профиля:");
                System.out.println("1. Сменить логин.");
                System.out.println("2. Сменить пароль.");
                System.out.println("3. Сменить тариф");
                int value = Integer.parseInt(scanner.nextLine());
                if (value == 1) {
                    System.out.println("Введите новый логин: ");
                    String userName = scanner.nextLine();
                    individual.setUserName(userName);
                    System.out.println("Поздравляем, вы успешно сменили логин на " + userName + ".");
                } else if (value == 2) {
                    System.out.println("Введите новый пароль: ");
                    String passw = scanner.nextLine();
                    individual.setPassword(passw);
                    System.out.println("Поздравляем, вы успешно сменили пароль на " + passw + ".");
                } else if (value == 3) {
                    System.out.println("Выберите тариф на который хотите сменить:");
                    tariffList.printTariffs();
                    int number2 = Integer.parseInt(scanner.nextLine());
                    if (number2 == 1 && individual.getTariff().getTariffs() != Operators.MTS) {
                        individual.setTariff(tariffList.getTariff(0));
                        System.out.println("Поздравляем, вы сменили тариф на " + Operators.MTS);
                    } else if (number2 == 2 && individual.getTariff().getTariffs() != Operators.MEGAFON) {
                        individual.setTariff(tariffList.getTariff(1));
                        System.out.println("Поздравляем, вы сменили тариф на " + Operators.MEGAFON);
                    } else if (number2 == 3 && individual.getTariff().getTariffs() != Operators.TELE2) {
                        individual.setTariff(tariffList.getTariff(2));
                        System.out.println("Поздравляем, вы сменили тариф на " + Operators.TELE2);
                    } else if (number2 > 3 || number2 < 1) {
                        System.out.println("Неверно введен номер тарифа.");
                    } else {
                        System.out.println("Вы уже используете этот тариф");
                    }
                }
            }
            if (change == 3 && !individual.getAccountLock()) {
                System.out.println("Введите номер, чтобы предоставить всю информацию по нему (+7-xxx-xxx-xx-xx):");
                String number3 = scanner.nextLine();
                while (number3.length() != 16 && number3.length() != 12) {
                    System.out.println("Неправильный формат телефона.");
                    System.out.println("Введите еще раз: ");
                    number3 = scanner.nextLine();
                }
                if (number3.equals(individual.getNumber())) {
                    System.out.println("Информация по данному номеру телефна: ");
                    System.out.println(individual.getPeopleD().getName() + ". " + individual.printInfo());
                } else {
                    System.out.println("Такого номера нет в базе данных.");
                }
            }
            if (change == 4 && !individual.getAccountLock()) {
                System.out.println("Остаток баланса: " + individual.getBalance());
                System.out.println("Остаток трафика: " + individual.getTariff().getTraffic());


            }
            if (change == 5 && !individual.getAccountLock()) {
                System.out.println("На какую сумму желаете пополнить ваш баланс?");
                int rub = Integer.parseInt(scanner.nextLine());
                individual.setBalance(rub);
                System.out.println("Вы пополнили баланс на "+rub+" рублей.");
                Date date5=new Date();
                Payment payment=new Payment(rub,date5);
                historyPayment.addPayment(payment);
            }
            if (change == 6 && !individual.getAccountLock()){
                System.out.println("История ваших платежей:");
                historyPayment.printHistory();
            }
            else if (change > 6 || change < 1) {
                System.out.println("Неправильный ввод");
            }
            if(individual.getAccountLock()) {
                System.out.println("Ваш аккаунт заблокирован. Разблокируйте его, если желаете продолжить" +
                        " пользоваться им.");
            }
        }
    }

}

