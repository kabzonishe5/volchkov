package account;

import java.util.Date;

public class Payment {
    private int sum;
    private Date date;


    Payment(int sum, Date date) {
        this.sum = sum;
        this.date = date;
    }

    public String toString(){
        return "Платеж на сумму "+sum+" был зачислен "+date;
    }
}
