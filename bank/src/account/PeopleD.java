package account;

import java.util.Date;

public class PeopleD {
    private int id;
    private String name;
    private String address;
    private Date date;
    private People people;

    PeopleD(String name, int id, String address, Date date, People people) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.date = date;
        this.people = people;
    }

    String getName(){
        return name+" ,адрес проживания:"+address;
    }


    @Override
    public String toString() {
        return id + ". " + name + " " + people;
    }
}
