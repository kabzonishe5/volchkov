package account;

public class Tariff {
    Operators tariffs;
    private String traffic;
    private int speedOfInternet;

    Tariff(String traffic, int speedOfInternet, Operators tariffs) {
        this.traffic = traffic;
        this.speedOfInternet = speedOfInternet;
        this.tariffs = tariffs;
    }
    public Operators getTariffs(){
        return  tariffs;
    }
    public String getTraffic(){
        return traffic;
    }

    public int getSpeedOfInternet() {
        return speedOfInternet;
    }

    @Override
    public String toString() {
        return tariffs + " скорость интернета " + speedOfInternet + " трафик " + traffic;
    }

}
